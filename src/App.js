import React, { Component } from "react";
import todosList from "./todos.json";

class App extends Component {
  
  state = {
    todos: todosList,
  };
  
  newTodo = (event) => {
    let boxValue = event.target.value

    if(event.keyCode === 13)
        {
        //got help from marcus with this syntax  
         this.setState((state) => {return {...state, todos: [...state.todos, {
          "userId": 1,
          "id": this.state.todos.length + 1,
          "title": boxValue,
          "completed": false
        }]}})
        //-------------------------------------
        console.log(this.state)
        }
      }
  handleDelete = (event, todoID) => {
        console.log(todoID)
        console.log(this.state.todos)
        if(this.state.todos.length === 1)
        {
          this.setState({ todos: [] })
        }
        else
        { 
        const newTodos = this.state.todos.filter(
          todo => todo.id !== todoID
        )
        this.setState({ todos: newTodos })
        }
      }

  deleteAll = (event) => {
      const clearedTodos = this.state.todos.filter(
        todo => todo.completed === false
      )
      this.setState({ todos: clearedTodos })
       
    
  }
  handleToggle = (event, todoID) => {
    console.log("toggled")
    const todosCopy = [...this.state.todos]
    console.log(todosCopy)
    todosCopy[todoID - 1].completed = true;
    this.setState({ todos: todosCopy })

  };
  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input id = "new_todo" className="new-todo" placeholder="What needs to be done?" onKeyUp = {this.newTodo}/>
        </header>
        <TodoList todos={this.state.todos} delete = {this.handleDelete} complete = {this.handleToggle}/>
        <footer className="footer">
          <span className="todo-count">
          <strong>{this.state.todos.length}</strong> item(s) left
          </span>
          <button className="clear-completed" onClick = {this.deleteAll}>Clear completed</button>
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {

  //----- used vince's example code ----
  state = {
    completed: false,
    deleted: false,
    id: this.props.id
  }

  // handleToggle = (event, todoID) => {
  //   console.log("toggled")
  //   this.setState({completed: !this.state.completed})

  // };
  // handleDelete = () => {
  //   console.log("hello")
  //   this.setState({...this.state, deleted: true})
  //   console.log(this.state)
  // }
  //-----------------------------------
  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input className="toggle" type="checkbox" checked={this.props.completed} onChange={event => this.props.complete(event, this.props.id)} />
          <label>{this.props.title}</label>
          <button className="destroy" onClick = {event => this.props.delete(event, this.props.id)}/>
        </div>
      </li>
    );
    
    // else
    // {
    //   return(
    //   <>
    //   </>
    //   );
    // }
  }
}

class TodoList extends Component {
  render() {
    if(this.props.todos.length === 0)
    {
      return (
      <section className="main">
        <ul className="todo-list">
        </ul>
      </section>
      );
    }
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem id = {todo.id} title={todo.title} completed = {todo.completed} complete={this.props.complete} delete = {this.props.delete}/>
          ))}
        </ul>
      </section>
    );
  }
}

export default App;